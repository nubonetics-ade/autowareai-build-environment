#!/bin/bash

mkdir -p autoware.ai/src
cd autoware.ai

# Clone the source code
wget https://gitlab.com/autowarefoundation/autoware.ai/autoware/-/raw/master/autoware.ai.repos
vcs import src < autoware.ai.repos

source /opt/ros/melodic/setup.bash
sudo rosdep init
rosdep update
rosdep install -y --from . --ignore-src

# Build AutowareAI with CUDA support
sudo bash -c "source /opt/ros/melodic/setup.bash; \
              AUTOWARE_COMPILE_WITH_CUDA=1 colcon build --merge-install --install-base /opt/AutowareAI --cmake-args -DCMAKE_BUILD_TYPE=Release"
cd ..

# Artifact to make a Docker volume: opt.tar.gz
sudo mkdir -p /opt/AutowareAI/src
sudo rsync -r --links --copy-unsafe-links  ./autoware.ai /opt/AutowareAI/
sudo chmod -R og-w /opt/AutowareAI
sudo chmod -R oga+w /opt/AutowareAI/lib/runtime_manager
tar cfz opt.tar.gz /opt/AutowareAI
