UBUNTU_RELEASE_YEAR=18
ZED_SDK_MAJOR=3
ZED_SDK_MINOR=1
CUDA_MAJOR=10
CUDA_MINOR=2
ROS_WS=~/ros_ws

SDK:
	sudo apt-get update -y 
	sudo apt-get install --no-install-recommends lsb-release wget less udev sudo  build-essential cmake -y
	wget -O ZED_SDK_Linux_Ubuntu${UBUNTU_RELEASE_YEAR}.run https://download.stereolabs.com/zedsdk/${ZED_SDK_MAJOR}.${ZED_SDK_MINOR}/cu${CUDA_MAJOR}${CUDA_MINOR}/ubuntu${UBUNTU_RELEASE_YEAR}
	chmod +x ZED_SDK_Linux_Ubuntu${UBUNTU_RELEASE_YEAR}.run 
	./ZED_SDK_Linux_Ubuntu${UBUNTU_RELEASE_YEAR}.run -- silent skip_tools
	rm ZED_SDK_Linux_Ubuntu${UBUNTU_RELEASE_YEAR}.run
	sudo rm -rf /var/lib/apt/lists/*

PY:
	sudo apt-get update -y
	sudo apt-get install --no-install-recommends python3 python3-pip -y
	wget download.stereolabs.com/zedsdk/pyzed -O /usr/local/zed/get_python_api.py
	sudo python3 /usr/local/zed/get_python_api.py
	sudo python3 -m pip install numpy opencv-python *.whl
	sudo rm *.whl ; sudo rm -rf /var/lib/apt/lists/*

ROS_INIT:
	sudo rosdep init
	rosdep update --rosdistro ${ROS_DISTRO}

ROS:
	mkdir -p ${ROS_WS}/src
	cd ${ROS_WS}/src; sudo git clone https://github.com/stereolabs/zed-ros-wrapper.git
	cd .. ; sudo apt-get update -y
	. /opt/ros/${ROS_DISTRO}/setup.sh
	rosdep install --from-paths ${ROS_WS}/src --ignore-src -r -y
	sudo rm -rf /var/lib/apt/lists/*
	cd ${ROS_WS}; catkin_make -DCMAKE_BUILD_TYPE=Release -DCMAKE_LIBRARY_PATH=/usr/local/cuda/lib64/stubs -DCUDA_CUDART_LIBRARY=/usr/local/cuda/lib64/stubs -DCMAKE_CXX_FLAGS="-Wl,--allow-shlib-undefined"

clean:
	sudo rm -r ${ROS_WS}/src/zed-ros-wrapper